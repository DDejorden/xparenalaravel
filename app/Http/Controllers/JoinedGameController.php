<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JoinedGames;

class JoinedGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return JoinedGames::orderBy('created_at', 'DESC')->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JoinedGames
     */
    public function store(Request $request)
    {
        $newJoinedGame = new JoinedGames;
        $newJoinedGame->quest_title = $request->JoinedGames["quest_title"];
        $newJoinedGame->user_id = $request->JoinedGames["user_id"];
        $newJoinedGame->creator_id = $request->JoinedGames["creator_id"];
        $newJoinedGame->firstName = $request->JoinedGames["firstName"];
        $newJoinedGame->familyName = $request->JoinedGames["familyName"];
        $newJoinedGame->game_pin = $request->JoinedGames["game_pin"];
        $newJoinedGame->originalGameId = $request->JoinedGames["originalGameId"];
        $newJoinedGame->save();
        return $newJoinedGame;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
