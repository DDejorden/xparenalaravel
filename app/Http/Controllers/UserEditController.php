<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;

class UserEditController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('user-edit', ['user' => $user]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $user->nickname = $request->input('nickname');
        $user->firstName = $request->input('firstName');
        $user->familyName = $request->input('familyName');
        $user->email = $request->input('email');

        if ( ! $request->input('password') == '')
        {
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();
        return Redirect::to('/user-edit');
    }

}
