<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Details;
use App\Models\Quest;

class DetailsController extends Controller
{
    public function index(Request $request)
    {
        $gameId = $request->quests;
        return view('details', ['gameId'=>$gameId]);
    }
}
