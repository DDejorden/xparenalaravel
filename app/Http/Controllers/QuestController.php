<?php

namespace App\Http\Controllers;

use App\Models\Quest;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class QuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return quest::orderBy('created_at', 'DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Quest
     */
    public function store(Request $request)
    {
        $newQuest = new quest;
        $newQuest->quest_title = $request->quest["quest_title"];
        $newQuest->user_id = $request->quest["user_id"];
        $newQuest->firstName = $request->quest["firstName"];
        $newQuest->familyName = $request->quest["familyName"];
        $newQuest->game_pin = $request->quest["game_pin"];
        $newQuest->save();
        return $newQuest;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existingQuest = Quest::find($id);
        if ($existingQuest) {
            $existingQuest->completed = $request->quest['completed'] ? true : false;
            $existingQuest->completed_at = $request->quest['completed'] ? Carbon::now() : null;
            $existingQuest->save();
            return $existingQuest;
        }
        return "Quest not found.";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existingQuest = Quest::find($id);
        if ($existingQuest) {
            $existingQuest->delete();
            return "Quest verwijderd";
        }
        return "Quest not found.";
    }

    /**
     * Get a specified resource from storage.
     *
     * @param int $game_pin
     * @return \Illuminate\Http\Response
     */
    public function get($game_pin)
    {
        $existingQuest = Quest::where('game_pin', $game_pin)->first();
        if ($existingQuest) {
            return $existingQuest;
        }
        return "Invalid game pin";
    }

}
