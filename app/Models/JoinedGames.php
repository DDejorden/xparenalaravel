<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JoinedGames extends Model
{
    use HasFactory;

    protected $guarded =['creator_id','quest_title','user_id', 'firstName', 'familyName'];

    public function games()
    {
        return $this->hasMany(Quest::class);
    }
}
