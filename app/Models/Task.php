<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $guarded =['task_title','quest_id', 'points', 'description', 'criteria'];


    public function quest()
    {
        return $this->belongsTo(Quest::class);
    }
}
