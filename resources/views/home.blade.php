@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        </div>
    </div>
    <body class="antialiased">
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
        @include('partials.menu')
    @elseif((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.mobile-menu')
    @endif

    <div>
        <dashboard-component
            :user="{{json_encode($user)}}"
            :avatar="{{ json_encode($user->avatar_path) }}"
        >
        </dashboard-component>
    </div>
</div>
</body>
@endsection
