@extends('layouts.app')

@section('content')
<div class="container-register">
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
        @include('partials.menu')
    @elseif((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.mobile-menu')
    @endif
    <div class="row justify-content-center">
        <a href="{{ url('/') }}">
            <img class="login-logo-mobile" src="images/Logo_XP_arena700px.png" alt="">
        </a>

        <div class="col-md-8">
            <div class="card">
                <h1 class="card-header">{{ __('Login') }}</h1>
                    <p>Welcome back!</p>

                <div class="login-container">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3 email-login-container">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('E-Mail Address:') }}</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password:') }}</label>

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="function-container">
                            <div>
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>

                            <button type="submit" class="login-btn">
                                {{ __('LOGIN') }}
                            </button>
                        </div>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                    </form>
                </div>
            </div>
        </div>
        <div class="login-create-account">
            <h2>Create account</h2>
            <p>Don’t have an account yet? No problem, just click the button below.</p>
            <a class="create-account-link" href="{{ url('/register') }}">CREATE ACCOUNT</a>
        </div>
    </div>
</div>
@endsection
