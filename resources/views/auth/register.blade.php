@extends('layouts.app')

@section('content')
<div class="container">
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
        @include('partials.menu')
    @elseif((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.mobile-menu')
    @endif
    <div class="card register-card">
        <h1 class="card-header">{{ __('Create account') }}</h1>
        <p>Cool! First we need some information for your account.</p>
        <div class="register-container">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="name-contanier">
                <div class="register-field-container">
                    <label for="firstName" class="register-label">{{ __('First name:') }}</label>

                    <input id="name" type="text" placeholder="Your name" class="register-input form-control @error('firstName') is-invalid @enderror" name="firstName" value="{{ old('firstName') }}" required autocomplete="firstName" autofocus>
                </div>
                        @error('firstName')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    <div class="register-field-container">

                    <label for="familyName" class="register-label">{{ __('Family name:') }}</label>

                    <input id="familyName" type="text" placeholder="Your family name" class="register-input form-control @error('familyName') is-invalid @enderror" name="familyName" value="{{ old('familyName') }}" required autocomplete="familyName" autofocus>

                    </div>
                        @error('familyname')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                    <div class="register-field-container">

                        <label for="email" class="register-label">{{ __('E-Mail:') }}</label>

                        <input id="email" type="email" placeholder="Your e-mail" class="register-input form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                <div class="password-container">

                <div class="register-field-container">

                    <label for="password" class="register-label">{{ __('Password:') }}</label>

                    <input id="password" type="password" class="register-input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                </div>
                    @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                <div class="register-field-container">

                    <label for="password-confirm" class="register-label">{{ __('Repeat:') }}</label>

                    <input id="password-confirm" type="password" class="register-input form-control" name="password_confirmation" required autocomplete="new-password">

                </div>

                </div>
                    <button type="submit" class="register-button">
                            {{ __('Send') }}
                    </button>
            </form>
        </div>
    </div>
</div>
@endsection
