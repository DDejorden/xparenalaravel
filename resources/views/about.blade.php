<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>About Xparena</title>
</head>

@if((new \Jenssegers\Agent\Agent())->isDesktop())
    @include('partials.menu')
@elseif((new \Jenssegers\Agent\Agent())->isMobile())
    @include('partials.mobile-menu')
@endif

<div id="app">
    <about-component></about-component>
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</html>
