<div class="nav-container">
    @if (Route::has('login'))
        <div class="home-nav">
            @auth
                <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Dashboard</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>
                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                @endif
            @endauth
                <a href="{{ url('/about') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">About</a>
        </div>
@endif
