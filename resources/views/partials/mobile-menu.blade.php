<div class="mobile-nav-container">
    <ul class="navbar-nav">
        <!-- Authentication Links -->
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <span></span>
                <span></span>
                <span></span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            @guest
                <div class="background-transparent"></div>
            @if (Route::has('login'))
                    <a class="dropdown-item text-sm text-gray-700 dark:text-gray-500 underline" href="{{ route('login') }}">{{ __('Login') }}</a>
            @endif
                <a href="{{ url('/about') }}" class="dropdown-item text-sm text-gray-700 dark:text-gray-500 underline">About</a>
                <a href="{{ url('/') }}" class="dropdown-item text-sm text-gray-700 dark:text-gray-500 underline">Home</a>

            @if (Route::has('register'))
                        <a class="dropdown-item text-sm text-gray-700 dark:text-gray-500 underline" href="{{ route('register') }}">{{ __('Register') }}</a>
            @endif
                </div>
        </li>
        @else
            <div class="background-transparent"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <a href="{{ url('/about') }}" class="dropdown-item text-sm text-gray-700 dark:text-gray-500 underline">About</a>
                    <a href="{{ url('/dashboard') }}" class="dropdown-item text-sm text-gray-700 dark:text-gray-500 underline">Dashboard</a>
                    <a href="{{ url('/') }}" class="dropdown-item text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
            </li>
        @endguest
    </ul>
    @guest
    @if(Route::has('login'))
        <a class="profile-pic" href="#"><img src="" alt=""></a>
    @endif
    @else
        <a class="profile-pic" href="{{ url('/user-edit') }}"><img src="storage/{{ \Illuminate\Support\Facades\Auth::user()->avatar_path }}" alt=""></a>
    @endguest
</div>




