<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Welcome!</title>
    </head>
    <body class="antialiased">
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
        @include('partials.menu')
    @elseif((new \Jenssegers\Agent\Agent())->isMobile())
        @include('partials.mobile-menu')
    @endif
    </body>
    <div id="app">
        <home-component
            register-route="{{route('register')}}"
        ></home-component>
    </div>
    <script src="{{ mix('/js/app.js') }}"></script>
</html>
