<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Contact us</title>
</head>
@if((new \Jenssegers\Agent\Agent())->isDesktop())
    @include('partials.menu')
@elseif((new \Jenssegers\Agent\Agent())->isMobile())
    @include('partials.mobile-menu')
@endif
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
<div id="app">
    <contact-component></contact-component>
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</div>
</html>
