<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Edit your user profile</title>
</head>
@if((new \Jenssegers\Agent\Agent())->isDesktop())
    @include('partials.menu')
@elseif((new \Jenssegers\Agent\Agent())->isMobile())
    @include('partials.mobile-menu')
@endif

<div id="app">
    <form class="profile-pic-nick" method="POST" action="{{ route('avatar', $user) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
    <div class="profile-pic-container">
        <label id="image-input-label" for="file-input">
            <img class="upload-icon" src="images/UI/download-icon.png" alt="Upload icon">
            <img class="current-profile-pic" alt="" src="<?php echo asset("storage/$user->avatar_path")?>"/>
        </label>
        <input id="file-input" type="file" name="avatar">
        <button class="save-btn" type="submit">SAVE</button>
    </div>



    </form>

    <form class="change-profile" method="post" action="{{route('updateUser', $user)}}">
        {{ csrf_field() }}
        {{ method_field('patch') }}

        <div class="nickname-container">
            <label for="nickname">Nickname:</label>
            <input id="nickname" type="text" name="nickname" value="{{ $user->nickname }}" />
        </div>

        <div class="first-name-container">
            <label for="first-name">First name:</label>
            <input id="first-name" type="text" name="firstName" value="{{ $user->firstName }}" />
        </div>

        <div class="family-name-container">
            <label for="family-name">Family name:</label>
            <input id="family-name" type="text" name="familyName"  value="{{ $user->familyName }}" />
        </div>

        <div class="email-container">
            <label for="email">E-mail:</label>
            <input id="email" type="email" name="email"  value="{{ $user->email }}" />
        </div>

        <div class="password-container">
            <label for="password">Password:</label>
            <input id="password" type="password" name="password" />
        </div>

        <button class="save-personal" type="submit">SAVE</button>
    </form>
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</html>
