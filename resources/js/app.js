/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import vuetify from "../../src/plugins/vuetify";

import router from './router/index'
import store from './store/index'
import App from './App.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faPlusSquare, faTrash, } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faPlusSquare, faTrash)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueRouter)

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.mixin({
    methods: {
        isMobile() {
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                return true
            } else {
                return false
            }
        }
    }
})

Vue.component('home-component', require('./components/Pages/Home').default);
Vue.component('about-component', require('./components/Pages/About').default);
Vue.component('contact-component', require('./components/Pages/Contact').default);
Vue.component('dashboard-component', require('./components/Pages/Dashboard').default);
Vue.component('details-component', require('./components/Organisms/DetailsOrganism').default);
Vue.component('user-edit-component', require('./components/Pages/UserEdit').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store,
    vuetify,
    components: { App }
}).$mount('#app');

