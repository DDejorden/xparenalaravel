import Details from "../components/Organisms/DetailsOrganism";
import Home from "../pages/Home.vue";
import About from "../pages/About.vue"
import TaskDetailsOrganism from "../components/Organisms/TaskDetailsOrganism";
import JoinedDetails from "../components/Organisms/JoinedDetailsOrganism";

const routes = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/about',
        component: About,
        name: 'about'
    },
    {
        path: '/details/:gameId',
        component: Details,
        name: 'details',
        props: true
    },
    {
        path: '/taskdetails/:taskId',
        component: TaskDetailsOrganism,
        name: 'taskdetails',
        props: true
    },
    {
        path: '/joineddetails/:gameId',
        component: JoinedDetails,
        name: 'joineddetails',
        props: true
    },
    {
        path: '/joinedtaskdetails/:taskId',
        component: TaskDetailsOrganism,
        name: 'joinedtaskdetails',
        props: true
    }
]

export default routes;
