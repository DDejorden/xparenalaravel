<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJoinedGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joined_games', function (Blueprint $table) {
            $table->id();
            $table->string("quest_title");
            $table->foreignId('user_id');
            $table->foreignId('creator_id');
            $table->foreignId('originalGameId');
            $table->integer('game_pin')->nullable();
            $table->string('firstName')->default(false);
            $table->string('familyName')->default(false);
            $table->boolean('completed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('joined_games');
    }
}
