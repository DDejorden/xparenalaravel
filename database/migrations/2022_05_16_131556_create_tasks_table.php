<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('quest_id')->nullable();
            $table->foreign('quest_id')->references('id')->on('quests')->onDelete('cascade');
            $table->string('task_title');
            $table->string('description');
            $table->integer('points');
            $table->string('criteria1');
            $table->string('criteria2');
            $table->string('criteria3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
