<?php

namespace Database\Factories;

use App\Models\Quest;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'quest_id' => Quest::factory(),
            'task_title' => $this->faker->jobTitle(),
            'description' => $this->faker->realText(),
            'criteria1' => $this->faker->jobTitle(),
            'criteria2' => $this->faker->jobTitle(),
            'criteria3' => $this->faker->jobTitle(),
            'points' => $this->faker->numberBetween(20, 1000)
        ];
    }
}
