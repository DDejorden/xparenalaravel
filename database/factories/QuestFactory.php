<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'quest_title' => $this->faker->jobTitle(),
            'firstName' => $this->faker->firstName(),
            'familyName' => $this->faker->lastName(),
        ];
    }
}
