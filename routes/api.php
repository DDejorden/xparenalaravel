<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuestController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserEditController;
use App\Http\Controllers\JoinedGameController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/quests', [QuestController::class, 'index']);
Route::get('/tasks', [TaskController::class, 'index']);
Route::get('/joinedGame', [JoinedGameController::class, 'index']);

Route::prefix('/quest')->group( function () {
    Route::post('/store', [QuestController::class, 'store']);
    Route::put('/{id}', [QuestController::class, 'update']);
    Route::delete('/{id}', [QuestController::class, 'destroy']);
    Route::get('/{id}', [QuestController::class, 'get']);
});

Route::prefix('/joinedGame')->group( function () {
    Route::post('/store', [JoinedGameController::class, 'store']);
//    Route::put('/{id}', [QuestController::class, 'update']);
//    Route::delete('/{id}', [QuestController::class, 'destroy']);
//    Route::get('/{game_pin}', [QuestController::class, 'get']);
});

Route::prefix('/task')->group( function () {
    Route::post('/store', [TaskController::class, 'store']);
    Route::put('/{id}', [TaskController::class, 'update']);
    Route::delete('/{id}', [TaskController::class, 'destroy']);
});

Route::prefix('/account')->group(function () {
    Route::put('/{id}', [UserEditController::class, 'update']);
});
