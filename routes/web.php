<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\UserAvatarController;
use App\Http\Controllers\UserEditController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('detect', function () {
    $agent = new \Jenssegers\Agent\Agent;

    $result = $agent->isMobile();

    if ($result)
        return "Yes, This is Mobile.";
    else
        return "No, This is not Mobile.";
});

Route::get('detect', function () {
    $agent = new \Jenssegers\Agent\Agent;

    $result = $agent->isDesktop();

    if ($result)
        return "Yes, This is Desktop.";
    else
        return "No, This is not Desktop.";
});
Route::get('detect', function () {
    $agent = new \Jenssegers\Agent\Agent;

    $result = $agent->isTablet();

    if ($result)
        return "Yes, This is Tablet.";
    else
        return "No, This is not Tablet.";
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
Route::get('/about', [App\Http\Controllers\AboutController::class, 'index'])->name('about');
Route::get('/details', [App\Http\Controllers\DetailsController::class, 'index'])->name('details');
Route::get('/user-edit', [App\Http\Controllers\UserEditController::class, 'index'])->name('user-edit');

Route::get('/images', [ImageController::class , 'show']);
Route::post('/upload',[ImageController::class, 'store']);

Route::post('api/users/{user}/avatar', [UserAvatarController::class, 'store'])->middleware('auth')->name('avatar');

Route::patch('/users/{user}/update', [UserEditController::class, 'update'])->name('updateUser');

